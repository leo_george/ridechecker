package com.fablab.balancechecker;


import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.LegendRenderer;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;


public class MainActivity extends AppCompatActivity {
private GraphView graphView;
private TextView tempView;
private  TextView speedView;
private  TextView alertView;
private  TextView latView;
private  TextView longView;

private  DatabaseReference motion;
private  DatabaseReference temp;
private  DatabaseReference speed;
private  DatabaseReference alert;
private  DatabaseReference lat;
private  DatabaseReference log;
private LineGraphSeries <DataPoint> series;

private double x=0;
private double y=0;
private double correctY=0;

final private String TAG="MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initGraph();
        initFirebase();
        plotGraph();

    }

    private void initGraph(){
        graphView=findViewById(R.id.graphview);
        tempView=findViewById(R.id.temperature);
        speedView=findViewById(R.id.speedView);
        alertView=findViewById(R.id.alertView);
        latView=findViewById(R.id.latView);
        longView=findViewById(R.id.logView);


        graphView.getViewport().setYAxisBoundsManual(true);
        graphView.getViewport().setXAxisBoundsManual(true);
        graphView.getViewport().setMinX(0.0);
        graphView.getViewport().setMaxX(1.0);
        graphView.getViewport().setMinY(-5.0);
        graphView.getViewport().setMaxY(5.0);
        graphView.getViewport().setScalable(true);
        graphView.getViewport().setScrollableY(true);
    }
    private void initFirebase(){
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        motion= database.getReference("motion");
        speed=database.getReference("speed");
        temp=database.getReference("temp");
        lat=database.getReference("lat");
        log=database.getReference("long");
        alert=database.getReference("alert");

    }
    private void plotGraph() {
        series=new LineGraphSeries<>();
        series.setTitle("motion");
        series.setDrawBackground(true);
        series.setColor(Color.argb(255,255,60,0));
        series.setBackgroundColor(Color.argb(100,204,119,119));
        series.setDrawDataPoints(true);
        graphView.getLegendRenderer().setVisible(true);
        graphView.getLegendRenderer().setAlign(LegendRenderer.LegendAlign.TOP);
        new Thread(new Runnable() {
            @Override
            public void run() {
                while(true) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            series.appendData(new DataPoint(x, y), true, 1000);
                            graphView.removeAllSeries();
                            graphView.addSeries(series);
                        }
                    });
                    x ++;
                    motion.addValueEventListener(new ValueEventListener() {

                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            Long value = dataSnapshot.getValue(Long.class);
                            correctY=Double.parseDouble(value.toString());
                                y=correctY;


                            Log.d(TAG, "Value is: " + value);
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            y=0;
                            Log.w(TAG, "Failed to read value.", databaseError.toException());
                        }
                    });

                    temp.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            Double temp=dataSnapshot.getValue(Double.class);
                            tempView.setText("Temperature: "+String.valueOf(temp)+"°C");
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            tempView.setText("Temperature: 0°C");
                        }
                    });
                    speed.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            Double speed=dataSnapshot.getValue(Double.class);
                            speedView.setText("Speed: "+String.valueOf(speed)+"Km/hr");
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            speedView.setText("Speed: 0 Km/hr");
                        }
                    });
                    alert.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            Double alert=dataSnapshot.getValue(Double.class);
                            if(alert==1.0){
                                alertView.setText("---WARNING---");
                            }else {
                                alertView.setText("");
                            }
                        }
                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                    lat.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            Double lat=dataSnapshot.getValue(Double.class);
                            latView.setText("Lat: "+lat);
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                    log.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            Double log=dataSnapshot.getValue(Double.class);
                            longView.setText("Long: "+log);
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        ).start();
    }


}
